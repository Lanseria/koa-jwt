'use strict';
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const Koa = require('koa');
const router = require('koa-router')();

const app = new Koa();

/**
 * After you login and get a token you can access
 * this (and any other non public endpoint) with:
 * curl -X GET -H "Authorization: Bearer INSERT_TOKEN_HERE" http://localhost:9000/sacred
 */

router.get('/api/v1', async (ctx) => {
  ctx.body = 'Hello '
});

app.use(router.routes());
app.use(router.allowedMethods());

module.exports = app;